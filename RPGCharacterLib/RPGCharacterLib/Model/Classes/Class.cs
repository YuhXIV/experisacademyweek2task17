﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes
{
    public abstract class Class
    {
        private string className, job;
        private int strPRC, intPRC, dexPRC, agiPRC;
        private int baseStr, baseInt, baseDex, baseLuk;
        private int armourRating;

        public string ClassName { get => className; set => className = value; }
        public string Job { get => job; set => job = value; }
        public int StrPRC { get => strPRC; set => strPRC = value; }
        public int IntPRC { get => intPRC; set => intPRC = value; }
        public int DexPRC { get => dexPRC; set => dexPRC = value; }
        public int AgiPRC { get => agiPRC; set => agiPRC = value; }
        public int BaseStr { get => baseStr; set => baseStr = value; }
        public int BaseInt { get => baseInt; set => baseInt = value; }
        public int BaseDex { get => baseDex; set => baseDex = value; }
        public int BaseLuk { get => baseLuk; set => baseLuk = value; }
        public int ArmourRating { get => armourRating; set => armourRating = value; }

        public abstract string AttackMove();
    }
}
