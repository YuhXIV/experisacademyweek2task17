﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubThief
{
    public class Shadower : Thief
    {
        public Shadower() : base()
        {
            Job = "Shadower";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }

        public override string AttackMove()
        {
            return "Ultimate jutsu: Kirin Kami";
        }
    }
}
