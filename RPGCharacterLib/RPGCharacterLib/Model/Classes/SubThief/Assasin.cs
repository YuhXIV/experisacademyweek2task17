﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubThief
{
    public class Assasin : Thief
    {
        public Assasin() : base()
        {
            Job = "Assasin";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Ansatsu: One-hit kill";
        }
    }
}
