﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes
{
    public class Mage : Class
    {
        public Mage() : base()
        {
            ClassName = "Mage";
            Job = " ";
            BaseStr = 5;
            BaseDex = 5;
            BaseInt = 12;
            BaseLuk = 5;
            StrPRC = 1;
            DexPRC = 1;
            IntPRC = 4;
            AgiPRC = 1;
            ArmourRating = 5;
        }
        public override string AttackMove()
        {
            return "Energy ball";
        }
    }
}
