﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes
{
    public class Archer : Class
    {
        public Archer() : base()
        {
            ClassName = "Archer";
            Job = " ";
            BaseStr = 5;
            BaseDex = 10;
            BaseInt = 5;
            BaseLuk = 7;
            StrPRC = 1;
            DexPRC = 3;
            IntPRC = 1;
            AgiPRC = 2;
            ArmourRating = 10;
        }
        public override string AttackMove()
        {
            return "Volleeeeeey!";
        }
    }
}
