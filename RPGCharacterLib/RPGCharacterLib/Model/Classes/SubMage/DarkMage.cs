﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubMage
{
    public class DarkMage : Mage
    {
        public DarkMage() : base()
        {
            Job = "Dark Mage";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Black Hole";
        }
    }
}
