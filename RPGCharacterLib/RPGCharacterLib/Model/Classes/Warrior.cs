﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes
{
    public class Warrior : Class
    {
        public Warrior() : base()
        {
            ClassName = "Warrior";
            Job = " ";
            BaseStr = 12;
            BaseDex = 5;
            BaseInt = 5;
            BaseLuk = 5;
            StrPRC = 4;
            DexPRC = 1;
            IntPRC = 1;
            AgiPRC = 1;
            ArmourRating = 20;
        }
        public override string AttackMove()
        {
            return "Slash";
        }
    }
}
