﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubWarrior
{
    public class DarkKnight : Warrior
    {
        public DarkKnight() : base()
        {
            Job = "Dark Knight";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Abyssal strike";
        }
    }
}
