﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubWarrior
{
    public class Lancer : Warrior
    {
        public Lancer() : base()
        {
            Job = "Lancer";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Dragon head";
        }
    }
}
