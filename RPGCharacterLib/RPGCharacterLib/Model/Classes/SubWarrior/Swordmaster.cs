﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubWarrior
{
    public class Swordmaster : Warrior
    {
        public Swordmaster() : base()
        {
            Job = "Swordmaster";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Nitoryu: Nigiri";
        }
    }
}
