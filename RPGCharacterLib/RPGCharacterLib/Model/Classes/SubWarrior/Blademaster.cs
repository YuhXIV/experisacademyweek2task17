﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes.SubWarrior
{
    public class Blademaster : Warrior
    {
        public Blademaster() : base()
        {
            Job = "Blademaster";
            StrPRC += 1;
            DexPRC += 1;
            IntPRC += 1;
            AgiPRC += 1;
        }
        public override string AttackMove()
        {
            return "Ikken hissatsu: Iaijutsu";
        }
    }
}
