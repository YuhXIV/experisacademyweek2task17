﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model.Classes
{
    public class Thief : Class
    {
        public Thief() : base()
        {
            ClassName = "Thief";
            Job = " ";
            BaseStr = 7;
            BaseDex = 5;
            BaseInt = 5;
            BaseLuk = 10;
            StrPRC = 2;
            DexPRC = 1;
            IntPRC = 1;
            AgiPRC = 3;
            ArmourRating = 15;
        }
        public override string AttackMove()
        {
            return "Konohagakure Hiden Taijutsu Ōgi: Sennen Goroshi!!";
        }
    }
}
