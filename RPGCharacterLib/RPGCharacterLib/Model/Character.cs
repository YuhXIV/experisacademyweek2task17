﻿using RPGCharacterLib.Model.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterLib.Model
{
    public class Character
    {
        private bool alive;
        private Class c;
        private string name;
        private int healthPoint, currentHealthPoint;
        private int manaPoint, currentManaPoint;
        private int str, dex, intint, agi;
        private int baseDmg;
        private int level, exp;
        private int armourRating;
        public Character(Class c, string name, string gender)
        {
            this.level = 1;
            this.c = c;
            this.name = name;
            this.Gender = gender;
            this.str = c.BaseStr;
            this.intint = c.BaseInt;
            this.dex = c.BaseDex;
            this.agi = c.BaseLuk;
            this.baseDmg = str * c.StrPRC + agi * c.AgiPRC + dex * c.DexPRC + intint * c.IntPRC;
            this.healthPoint = str * 50;
            this.manaPoint = intint * 30;
            this.currentHealthPoint = healthPoint;
            this.currentManaPoint = manaPoint;
            alive = true;
            this.armourRating = c.ArmourRating;
        }

        public string Name { get => name; }
        public string Gender { get; }

        public Class Class { get => c; set => c = value; }
        public int Level { get => level; }
        public int Exp { get => exp; set => exp += value; }
        public int Str { get => str; }
        public int Dex { get => dex; }
        public int Intint { get => intint; }
        public int Agi { get => agi; }
        public int BaseDmg { get => baseDmg; }
        public int HealthPoint { get => healthPoint; set => healthPoint += value; }
        public int CurrentHealthPoint { get => currentHealthPoint; }
        public int ManaPoint { get => manaPoint; set => manaPoint += value; }
        public int CurrentManaPoint { get => currentManaPoint; }

        /// <summary>
        /// Add the amount of exp to the character
        /// </summary>
        /// <param name="exp"></param>
        public void AddEXP(int exp)
        {
            this.exp += exp;
        }

        /// <summary>
        /// Character levels up!!! all stats increase!
        /// </summary>
        public void LevelUp()
        {
            level += 1;
            this.str += 2 + (int)(str * 0.01 * c.StrPRC);
            this.intint += 2 + (int)(intint * 0.01 * c.IntPRC);
            this.dex += 2 + (int)(dex * 0.01 * c.DexPRC);
            this.agi += 2 + (int)(agi * 0.01 * c.AgiPRC);
            this.healthPoint = str * 50;
            this.manaPoint = intint * 30;
            this.baseDmg = str * c.StrPRC + agi * c.AgiPRC + dex * c.DexPRC + intint * c.IntPRC;
            this.currentHealthPoint = healthPoint;
            this.currentManaPoint = manaPoint;
        }

        /// <summary>
        /// At level 30 character has the possibility of having a job advancement, his basic stats will be growing much higher
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool UpgradeClass(Class c)
        {
            if (level >= 30)
            {
                Console.WriteLine($"All requirment met, class advanced to {c.Job}");
                Class = c;
                return true;
            }
            Console.WriteLine($"Need to be level 30 to advance to {c.Job}");
            return false;
        }

        /// <summary>
        /// Character takes damage, decreasing health
        /// </summary>
        /// <param name="damage"></param>
        public void TakeDamage(int damage)
        {
            this.currentHealthPoint -= damage - armourRating;
            if (currentHealthPoint <= 0)
            {
                alive = false;
            }
        }

        /// <summary>
        /// Character heals, cant heal over the max health
        /// </summary>
        /// <param name="heal"></param>
        public void Heal(int heal)
        {
            if (this.currentHealthPoint + heal > healthPoint)
            {
                currentHealthPoint = healthPoint;
            }
            else
            {
                this.currentHealthPoint += heal;
            }
        }

        /// <summary>
        /// Character moves in a direction
        /// </summary>
        /// <param name="direction"></param>
        public void Move(string direction)
        {
            Console.WriteLine($"{name} just moved {direction}....");
        }

        /// <summary>
        /// Character attacks! doing damage to monster
        /// </summary>
        /// <returns></returns>
        public int Attack()
        {
            Console.WriteLine($"{c.AttackMove()} ");
            return baseDmg;
        }
        /// <summary>
        /// checking if the character is still alive
        /// </summary>
        /// <returns></returns>
        public bool IsAlive()
        {
            return alive;
        }
        /// <summary>
        /// Character has the possibility to revive
        /// </summary>
        public void Revive()
        {
            alive = true;
        }
        /// <summary>
        /// Return a string with all the character information
        /// </summary>
        /// <returns></returns>
        public string CharacterInfo()
        {
            return $"     Character info {System.Environment.NewLine}" +
                $"Level:               {level} {System.Environment.NewLine}" +
                $"Name:                {name} {System.Environment.NewLine}" +
                $"Gender:              {Gender} {System.Environment.NewLine}" +
                $"Class:               {c.ClassName} | Job: {c.Job} {System.Environment.NewLine}" +
                $"Health points (HP):  {healthPoint}/{currentHealthPoint} {System.Environment.NewLine}" +
                $"Mana points (MP):    {manaPoint}/{currentManaPoint} {System.Environment.NewLine}" +
                $"Base Damage:         {baseDmg} {System.Environment.NewLine}" +
                $"Strength (STR):      {str} {System.Environment.NewLine}" +
                $"Dexterity (DEX):     {dex} {System.Environment.NewLine}" +
                $"Intelligence (INT):  {intint} {System.Environment.NewLine}" +
                $"Agility (AGI):       {agi} {System.Environment.NewLine}" +
                $"Armour Rating:       {armourRating} {System.Environment.NewLine}";
        }
    }
}
