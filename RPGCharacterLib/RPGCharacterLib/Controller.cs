﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacterLib.Model;
using RPGCharacterLib.Model.Classes;
using RPGCharacterLib.Model.Classes.SubArcher;
using RPGCharacterLib.Model.Classes.SubMage;
using RPGCharacterLib.Model.Classes.SubThief;
using RPGCharacterLib.Model.Classes.SubWarrior;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace RPGCharacterLib
{

    public class Controller
    {
        Character character;
        List<Class> mainClasses = new List<Class>();
        List<Class> subClasses = new List<Class>();

        public Controller()
        {
            Thief thief = new Thief();
            Archer archer = new Archer();
            Warrior warrior = new Warrior();
            Mage mage = new Mage();
            Character c1 = new Character(thief, "Kjetil Huy", "Male");
            Character c2 = new Character(archer, "Markus", "Male");
            Character c3 = new Character(warrior, "Oorjan", "Female");
            Character c4 = new Character(mage, "Comparable69", "Female");
            //CreateCharacter(thief, "Magdeli Asplin", "Female");
            //Console.WriteLine(character.CharacterInfo());

            Ranger ranger = new Ranger();
            Shadower shadower = new Shadower();
            IceMage iceMage = new IceMage();

            AdvanceClass(c1, shadower);
            AdvanceClass(c2, ranger);
            AdvanceClass(c4, iceMage);

            for (int i = 0; i < 20; i++)
            {
                c3.LevelUp();
                c1.LevelUp();
            }
            //AdvanceClass(character, shadower);
            //Console.WriteLine(character.CharacterInfo());

            Console.WriteLine(c1.CharacterInfo());
            Console.WriteLine(c2.CharacterInfo());
            Console.WriteLine(c3.CharacterInfo());
            Console.WriteLine(c4.CharacterInfo());

            WriteCharacterInfo(c1, @"C:\Users\kjthuy\Documents\");
        }

        public void AllMainClass()
        {
            Thief thief = new Thief();
            Archer archer = new Archer();
            Warrior warrior = new Warrior();
            Mage mage = new Mage();

            mainClasses.Add(thief);
            mainClasses.Add(archer);
            mainClasses.Add(warrior);
            mainClasses.Add(mage);

            Shadower shadower = new Shadower();
            Assasin sin = new Assasin();
            Ranger ranger = new Ranger();
            Sniper sniper = new Sniper();
            DarkMage darkMage = new DarkMage();
            FireMage fireMage = new FireMage();
            IceMage iceMage = new IceMage();
            LightMage lightMage = new LightMage();
            LightningMage lightningMage = new LightningMage();
            WindMage windMage = new WindMage();
            Blademaster blademaster = new Blademaster();
            DarkKnight darkKnight = new DarkKnight();
            Guardian guardian = new Guardian();
            Lancer lancer = new Lancer();
            Swordmaster swordmaster = new Swordmaster();

            subClasses.Add(shadower);
            subClasses.Add(sin);
            subClasses.Add(ranger);
            subClasses.Add(sniper);
            subClasses.Add(darkMage);
            subClasses.Add(fireMage);
            subClasses.Add(iceMage);
            subClasses.Add(lightMage);
            subClasses.Add(lightningMage);
            subClasses.Add(windMage);
            subClasses.Add(blademaster);
            subClasses.Add(darkKnight);
            subClasses.Add(guardian);
            subClasses.Add(lancer);
            subClasses.Add(swordmaster);

        }
        /// <summary>
        /// Filter all the subclass associated with the characters current class 
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public List<Class> SubClass(Character character)
        {
            return subClasses.FindAll(cls => cls.GetType().IsSubclassOf(character.Class.GetType()));
        }

        /// <summary>
        /// Creates the Main character
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <param name="gender"></param>
        public void CreateCharacter(Class c, string name, string gender)
        {
            this.character = new Character(c, name, gender);
            Console.WriteLine($"Character {name} created..");
        }
        /// <summary>
        /// Let the character advance his class, a tweak is done as he lvl up automatically to required lvl
        /// </summary>
        /// <param name="c"></param>
        /// <param name="cl"></param>
        public void AdvanceClass(Character c, Class cl)
        {
            for (int i = 0; i < 29; i++)
            {
                c.LevelUp();
            }
            c.UpgradeClass(cl);
        }
        /// <summary>
        /// The character attacks!
        /// </summary>
        /// <param name="c"></param>
        public void Attack(Character c)
        {
            int dmg = c.Attack();
            Console.WriteLine($"{c.Name} dealt {dmg} damage");
        }
        /// <summary>
        /// the character moves!
        /// </summary>
        /// <param name="c"></param>
        /// <param name="direction"></param>
        public void Move(Character c, string direction)
        {
            c.Move(direction);
        }
        /// <summary>
        /// Save the character info into a .txt file
        /// </summary>
        /// <param name="c"></param>
        /// <param name="filePath"></param>
        public void WriteCharacterInfo(Character c, string filePath)
        {
            string curFile = filePath + "Character_info.txt";
            if (File.Exists(curFile))
            {
                File.Delete(curFile);
                using (StreamWriter file = new StreamWriter(curFile))
                {
                    file.WriteLine(c.CharacterInfo());
                }
            }
            else
            {
                using (FileStream fs = File.Create(curFile))
                {
                    // Add some text to file    
                    Byte[] title = new UTF8Encoding(true).GetBytes("Character_info.txt");
                    fs.Write(title, 0, title.Length);
                }
                using (StreamWriter file = new StreamWriter(curFile))
                {
                    file.WriteLine(c.CharacterInfo());
                }
            }
        }
    }
}

